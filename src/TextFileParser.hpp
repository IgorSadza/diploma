/**
    @File:      TextParser.cpp
    @Purpose:   Parse file to section based on their header.
                Analyze given section and create key - value structure.    

    @Author:    Igor Sadza
    @Version:   1.0 - 30/01/19
                2.0 - 13/02/19
                3.0 - 17/02/19

*/

#ifndef TEXT_PARSER_HPP
#define TEXT_PARSER_HPP

#include "CommonIncludes.hpp"

class TextFileParser {
private:

  struct HeadSection {
    std::string m_header;
    std::string m_section;
  };

  struct KeyVariable {
    std::string m_key;
    std::string m_variable;
  };

public:
  // Containers.
  typedef std::vector<HeadSection> Sections;
  typedef std::vector<KeyVariable> Variables;
  typedef std::multimap<std::string, Variables> Registry;

private:
  // Variables.
  Sections      m_sections;
  Registry      m_variables;
  std::string   m_fileName;
  std::string   m_filePath;
  
  // Parase file to header and source. (HeadSource struct).
  void parseSources();

  // Parse lines of section in sequence and send them to parseKeyVariable() function.
  void parseVariables();

  // Add global variables to individual section.
  void parseGlobals();
  
public:
  // Open and check given path, call functions.
  TextFileParser(const std::string &t_filePath);

  // Get file name.
  std::string getName();

  // Get all sections.
  Sections getSections();

  // Get variables from section.
  Registry getVariables();

  // Transform string variable to chosen type.
  template <class Type> 
  static Type parseVariable(std::string t_Variable) {
    Type variable;
    for (int i = 0; i < sizeof(Type) / sizeof(1.0f); i++) {
      size_t commaSiqnPos = t_Variable.find_first_of(',');
      std::string value = t_Variable.substr(0, commaSiqnPos);
      t_Variable = t_Variable.substr(commaSiqnPos + 1, t_Variable.size());
      std::stringstream(value) >> variable[i];
    }
    return variable;
  } 

  // Parse file name from path.
  static std::string parseName(const std::string &t_textLine);
};

#endif // TEXT_PARSER_HPP