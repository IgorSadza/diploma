#ifndef STATE_MENU_HPP_
#define STATE_MENU_HPP_

#include "CommonIncludes.hpp"
#include "State.hpp"

class StateMenu : public State {
private:
public:
  StateMenu();
  GLvoid logic();
  GLvoid render();
};
#endif // STATE_MENU_HPP_