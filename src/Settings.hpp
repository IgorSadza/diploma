#ifndef SETTINGS_HPP
#define SETTINGS_HPP

#include "CommonIncludes.hpp"

namespace settings {
// window settings
extern const char *name;
extern const float width;
extern const float height;
extern const unsigned mouseKeys;
extern const GLFWwindow *window;
}; // namespace settings

#endif // SETTINGS_HPP