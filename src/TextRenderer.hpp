#ifndef TEXT_RENDERER_HPP
#define TEXT_RENDERER_HPP

#include "CommonIncludes.hpp"
#include "Object.hpp"

class TextRenderer {
private:
    TextRenderer();
    static unsigned     m_vao;
    static unsigned     m_vbo;
    static Shader       m_textShader;
public:
    static TextRenderer &init();
    static void render(Object *t_object);
};
#endif // TEXT_RENDERER_HPP