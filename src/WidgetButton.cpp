#include "WidgetButton.hpp"
#include "TextRenderer.hpp"
#include "ObjectRenderer.hpp"
#include "Cursor.hpp"
#include "Object.hpp"

WidgetButton::WidgetButton(TextFileParser::Variables t_variables)
    : Widget(t_variables) {

}

void WidgetButton::logic() {
    checkClick();
}

void WidgetButton::render() {
 ObjectRenderer::render(object());
 TextRenderer::render(object());
}

void WidgetButton::checkClick() {
    if (Object::CheckCollision(object()->rectangle(), Cursor::object()->rectangle())) {
        object()->rectangle()->color(0.90f);
        if (Cursor::key(GLFW_MOUSE_BUTTON_1)) {
            //object()->text()->position().y -= 10.0f;
        }
    }
    else {
        object()->rectangle()->color(1.0f);
    }
}