#ifndef WIDGET_BUTTON_HPP
#define WIDGET_BUTTON_CPP

#include "CommonIncludes.hpp"
#include "Widget.hpp"
#include "SharedResources.hpp"

class WidgetButton : public Widget {
    private:
        void checkClick();
    public:
        WidgetButton(TextFileParser::Variables t_variables);
        void render();
        void logic();
};

#endif //WIDGET_BUTTON_HPP