/**
    Shader.hpp
    Purpose: Build shader program from text file.

    @author Igor Sadza
    @version 1.0 - 30/01/19
*/

#ifndef SHADER_HPP
#define SHADER_HPP

#include "CommonIncludes.hpp"

class Shader {
private:
  // Values.
  unsigned m_shaderProgram;
  std::string m_shaderName;

  // Link specific shader with current shader program.
  void link(unsigned t_shaderID, unsigned t_programID);
  // Init spefific shader with current shader source.
  void init(unsigned t_shaderID, const std::string &t_source);

public:
  // Get parased shader source and create appropriate shaders programs.
  Shader(const std::string &t_path);

  // Default constructor.
  Shader();
  
  // Check the type, create array and send it to uniform. 
  template <class Type> 
  void sendVariable(const std::string &t_uniformName, Type t_value);

  // Get name of object.
  std::string name();
  // Use shader program.
  void useShader();
};

#include "Shader.hxx"

#endif // SHADER_HPP