/**
    File: Object.cpp
    Purpose: Base to render object.

    @author Igor Sadza
    @version 1.0 - 05/01/19
    @version 2.0 - 12/01/19
*/

#ifndef FONT_HPP
#define FONT_HPP

#include "CommonIncludes.hpp"

class Font {
public:
  struct Glyph {
    unsigned    m_id;
    glm::ivec2  m_size;
    glm::ivec2  m_bearing;
    GLint64     m_advance;
  };

private:
  typedef std::map<char, Glyph> Alphabet;

  Alphabet      m_alphabet;
  std::string   m_name;

public:
  Font();
  Font(const std::string &t_pathToFont);

  void font();
  void fontSize();

  std::string   name();
  Alphabet      alphabet();
};

#endif // FONT_HPP