#include "ParticlesGenerator.hpp"
#include "ResourceManager.hpp"
#include "SharedResources.hpp"

// ! ---------------------------------
// Particle

Particle::Particle()
  : Rectangle()
  , m_life(0.0f) {
}

// ! ---------------------------------
// ParticleGenerator

ParticleGenerator::ParticleGenerator(glm::vec2 t_position, glm::vec2 t_range, unsigned t_amount)
  : m_range(t_range)
    , m_amount(t_amount)  {

    for (int i = 0; i < m_amount; i++) {
      Particle particle;
      particle.position(t_position);
      particle.size(5.0f);
      particle.color(1.0f);
      m_particles.push_back(particle);
    }
}

ParticleGenerator::ParticleGenerator(Object &t_object, glm::vec2 t_range, unsigned t_amount)  
  : m_range(t_range)
  , m_amount(t_amount) {
    m_positionFrom = t_object.rectangle()->position();

    for (int i = 0; i < m_amount; i++) {
      Particle particle;
      m_particles.push_back(particle);
    }
}

void ParticleGenerator::updateParticle() {
  for (int i = 0; i < m_amount; i++) {
    float rand_colo = 0.5 + ((rand() % 100) / 100.0f);
    //m_particles[i].color() -= rand_colo;
    m_particles[i].m_life -= 0.1f * shared::delta_time;
    m_particles[i].position()->y += 4.0f + rand_colo;
  }
}

void ParticleGenerator::renderParticles() {

  updateParticle();
  for (auto& ite: m_particles) {
      ParticleRenderer::render(&ite);
    }
  respawnParticle();
}

Particle *ParticleGenerator::findFirstUnusedParticle() {
  for (auto& ite: m_particles) {
    if (ite.m_life <= 0.0f) {
      return &ite;
    }
  }
  return nullptr;
 }

void ParticleGenerator::respawnParticle() {
  if (findFirstUnusedParticle()) {

    glm::vec2 rand_pos((rand() % 800), 0.0f);

    float rand_colo = 0.1 + ((rand() % 100) / 100.0f);

    float d = 0.0f;
    Particle *ptr_particle = findFirstUnusedParticle();
    ptr_particle->position(rand_pos);
    ptr_particle->m_life = 1.0f;
    ptr_particle->color(rand_colo);
  }
}

// ! ---------------------------------
// ParticleRenderer

unsigned ParticleRenderer::m_vao;
unsigned ParticleRenderer::m_vbo;
Shader ParticleRenderer::m_shader;

ParticleRenderer::ParticleRenderer() {

  m_shader = Shader("res/shaders/particle.shader");

  float verticies[] = {
    0.0f, 0.0f, 0.0f, 0.0f, 
    1.0f, 0.0f, 1.0f, 0.0f, 
    0.0f, 1.0f, 0.0f, 1.0f, 

    1.0f, 1.0f, 1.0f, 1.0f,
    1.0f, 0.0f, 1.0f, 0.0f, 
    0.0f, 1.0f, 0.0f, 1.0f
  };

  glGenVertexArrays(1, &m_vao);
  glGenBuffers(1, &m_vbo);

  glBindVertexArray(m_vao);
  glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(verticies), &verticies, GL_STATIC_DRAW);

  glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void *)(0 * sizeof(float)));
  glEnableVertexAttribArray(0);

  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);
}

ParticleRenderer &ParticleRenderer::init() {
  static ParticleRenderer particleRenderer;
  return particleRenderer;
}

void ParticleRenderer::render(Particle *t_particle) {

  m_shader.useShader();

  glm::mat4 modelMatrix;
  modelMatrix = glm::translate(modelMatrix, glm::vec3(*t_particle->position(), 0.0f));
  modelMatrix = glm::scale(modelMatrix, glm::vec3(*t_particle->size(), 0.0f));

  m_shader.sendVariable("t_model", modelMatrix);
  m_shader.sendVariable("t_color", *t_particle->color());

  glBindVertexArray(m_vao);
    glDrawArrays(GL_TRIANGLES, 0, 6);
  glBindVertexArray(0); 
}