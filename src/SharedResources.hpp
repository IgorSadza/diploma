#ifndef SHARED_RESOURCES_HPP
#define SHARED_RESOURCES_HPP

#include "CommonIncludes.hpp"

namespace shared {
extern GLFWwindow *window;
extern float delta_time;
}; // namespace shared

#endif // SHARED_RESOURCES_HPP