#ifndef CURSOR_HPP
#define CURSOR_HPP

#include "CommonIncludes.hpp"
#include "Settings.hpp"
#include "Object.hpp"

class Cursor {
private:
    Cursor();
    static Object      m_object;
    static bool        m_mouseKeys[7];
public:
    static Cursor &init();
    static Object *object();

    static void key(int t_key, int t_action);
    static bool key(int t_key);
};

#endif //CURSOR_HPP