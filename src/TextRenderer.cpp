#include "TextRenderer.hpp"
#include "Shader.hpp"
#include "Font.hpp"

unsigned TextRenderer::m_vao;
unsigned TextRenderer::m_vbo;
Shader TextRenderer::m_textShader;

TextRenderer::TextRenderer() {
  m_textShader = Shader("res/shaders/text.shader");
  m_textShader.sendVariable("t_sampler", 0);

  glGenVertexArrays(1, &m_vao);
  glGenBuffers(1, &m_vbo);

  glBindVertexArray(m_vao);
  glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 6 * 4, NULL, GL_DYNAMIC_DRAW);

  glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (void * )(0 * sizeof(GLfloat)));
  glEnableVertexAttribArray(0);

  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);
}

TextRenderer &TextRenderer::init() {
  static TextRenderer instance;
  return instance;    
}

void TextRenderer::render(Object *t_object) {
  
  if (!t_object->text())  {
      return;
  }

  m_textShader.useShader();

  glActiveTexture(GL_TEXTURE0);
  glBindVertexArray(m_vao);

  float lineLenght = 0.0f;
  std::string::const_iterator ite;
  std::string textToRender = t_object->text()->text();
  
  for (ite = textToRender.begin(); ite != textToRender.end(); ite++) {

    Font::Glyph glyph = t_object->text()->font().alphabet()[*ite];
    Font::Glyph H_char = t_object->text()->font().alphabet()['H'];

    float xpos = t_object->text()->position().x + lineLenght + glyph.m_bearing.x;
    float ypos = t_object->text()->position().y + (H_char.m_size.y - glyph.m_bearing.y);

    float w = glyph.m_size.x;
    float h = glyph.m_size.y;

    float vertices[6][4] = {
        { xpos,     ypos + h,   0.0, 1.0 },
        { xpos + w, ypos,       1.0, 0.0 },
        { xpos,     ypos,       0.0, 0.0 },

        { xpos,     ypos + h,   0.0, 1.0 },
        { xpos + w, ypos + h,   1.0, 1.0 },
        { xpos + w, ypos,       1.0, 0.0 }
    };

    glBindTexture(GL_TEXTURE_2D, glyph.m_id);

      glBindBuffer(GL_ARRAY_BUFFER, m_vao);
        glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
      glBindBuffer(GL_ARRAY_BUFFER, 0);

        glDrawArrays(GL_TRIANGLES, 0, 6);

    glBindTexture(GL_TEXTURE_2D, 0);

    lineLenght += (glyph.m_advance >> 6);
  }   
}