#include "Text.hpp"
#include "ResourceManager.hpp"

Text::Text() {}

Text::Text(const std::string &t_renderText) : m_textToRender(t_renderText) {
  (ResourceManager<Font>::searchObject("04b_30.ttf"))
  ? m_font = ResourceManager<Font>::getObject("04b_30.ttf")
  : m_font = Font("res/fonts/04b_30.ttf");
}

void Text::font(const std::string &t_fontPath) { m_font = Font(t_fontPath); }

Font Text::font() { return m_font; }

void Text::text(const std::string &t_renderText) { m_textToRender = t_renderText; }

std::string Text::text() { return m_textToRender; }

void Text::textShif(Rectangle *t_objectRectangle) {

  float lineLenght;
  std::string::const_iterator ite;
  for (ite = m_textToRender.begin(); ite != m_textToRender.end(); ite++) {
    Font::Glyph glyph = m_font.alphabet()[*ite];
    lineLenght += (glyph.m_advance >> 6);
  }

  glm::vec2 textRect;
  textRect.x = lineLenght;
  textRect.y = m_font.alphabet()['H'].m_size.y;

  m_position.x = t_objectRectangle->position()->x + t_objectRectangle->size()->x / 2 - textRect.x / 2;
  m_position.y = t_objectRectangle->position()->y + t_objectRectangle->size()->y / 2 - textRect.y / 2;
}

glm::vec2 Text::position() { return m_position; }