#include "ObjectRenderer.hpp"
#include "ResourceManager.hpp"

unsigned ObjectRenderer::m_vao;
unsigned ObjectRenderer::m_vbo;

ObjectRenderer::ObjectRenderer() {
  
  float verticies[] = {
    0.0f, 0.0f, 0.0f, 0.0f, 
    1.0f, 0.0f, 1.0f, 0.0f, 
    0.0f, 1.0f, 0.0f, 1.0f, 

    1.0f, 1.0f, 1.0f, 1.0f,
    1.0f, 0.0f, 1.0f, 0.0f, 
    0.0f, 1.0f, 0.0f, 1.0f
  };

  glGenVertexArrays(1, &m_vao);
  glGenBuffers(1, &m_vbo);

  glBindVertexArray(m_vao);
  glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(verticies), &verticies, GL_STATIC_DRAW);

  glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void *)(0 * sizeof(float)));
  glEnableVertexAttribArray(0);

  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);

}

ObjectRenderer &ObjectRenderer::init() {
  static ObjectRenderer objectRenderer;
  return objectRenderer;
}

void ObjectRenderer::render(Object *t_object) {

  t_object->shader()->useShader();
  t_object->image()->bindImage();

  glm::mat4 modelMatrix;
  modelMatrix = glm::translate(modelMatrix, glm::vec3(*t_object->rectangle()->position(), 0.0f));
  modelMatrix = glm::scale(modelMatrix, glm::vec3(*t_object->rectangle()->size(), 0.0f));

  t_object->shader()->sendVariable("t_model", modelMatrix);
  t_object->shader()->sendVariable("t_color", *t_object->rectangle()->color());

  glBindVertexArray(m_vao);
    glDrawArrays(GL_TRIANGLES, 0, 6);
  glBindVertexArray(0); 
}