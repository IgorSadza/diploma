#include "Rectangle.hpp"

template <class Type> 
template <class Arg> 
Type Rectangle::Implementation<Type>::parsePointers(const Arg &t_arg, int t_argSize) {

  int returnTypeSize = sizeof(Type) / sizeof(1);
  auto *firstElement = (float *)((void *)t_arg.begin());

  Type result;

  for (int i = 0; i < returnTypeSize; i++) {
    (i < t_argSize)
    ? result[i] = *(firstElement++)
    : result[i] = *(firstElement-1);
  }

  return result;  
}

template <class Type> 
template <class... Args>
Type Rectangle::Implementation<Type>::createVector(Args... t_args) {

  auto arguments = {t_args...};
  int unitCount = 0;
  int vecCount = 0;


  for (auto &ite: arguments) {
    (sizeof(ite) / sizeof(float) == 1)
    ? unitCount++
    : vecCount++;
  }

  int unitVecSize = sizeof(*arguments.begin()) / sizeof(1.0f);

  return (vecCount >= 1)
  ? Implementation<Type>::parsePointers(arguments, unitVecSize) 
  : Implementation<Type>::parsePointers(arguments, unitCount);
}

template <class... Args> 
void Rectangle::size(Args... t_args) {
  m_size = Implementation<glm::vec2>::createVector(t_args...);
}

template <class... Args> 
void Rectangle::position(Args... t_args) {
  m_position = Implementation<glm::vec2>::createVector(t_args...);
}

template <class... Args> 
void Rectangle::color(Args... t_args) {
  m_color = Implementation<glm::vec4>::createVector(t_args...);
}