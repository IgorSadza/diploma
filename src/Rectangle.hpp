#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "CommonIncludes.hpp"

class Rectangle {
private:

  template<class Type>
  struct Implementation {
    template <class Arg>
    static Type parsePointers(const Arg &t_arg, int t_argSize);

    template <class... Args> 
    static Type createVector(Args... t_args);
  };

  glm::vec2     m_size;
  glm::vec2     m_position;
  glm::vec4     m_color;

public:
  // Constructor.
  Rectangle() : m_size(0.0f), m_position(0.0f), m_color(1.0f) {}

  // Seters.
  template <class... Args> 
  void size(Args... t_args);
  glm::vec2 *size() { return &m_size; }

  template <class... Args> 
  void position(Args... t_args);
  glm::vec2 *position() { return &m_position; }

  template <class... Args> 
  void color(Args... t_args);
  glm::vec4 *color() { return &m_color; }
};

#include "Rectangle.hxx"

#endif //RECTANGLE_HPP