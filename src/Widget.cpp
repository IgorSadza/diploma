#include "Widget.hpp"

Widget::Widget(TextFileParser::Variables t_variables) {
    for (auto &ite :t_variables) {
        if (ite.m_key == "Type") {
            if (ite.m_variable == "Rectangle") {
                m_object = Object(Object::Type::RECTANGLE);
            } else {
                m_object = Object(Object::Type::CIRCLE);
            }
        }
        if (ite.m_key == "RectangleSize") {
            m_object.rectangle()->size(TextFileParser::parseVariable<glm::vec2>(ite.m_variable));
        } else 
        if (ite.m_key == "RectanglePosition") {
            m_object.rectangle()->position(TextFileParser::parseVariable<glm::vec2>(ite.m_variable));
        } else 
        if (ite.m_key == "RectangleColor") {
            m_object.rectangle()->color(TextFileParser::parseVariable<glm::vec2>(ite.m_variable));
        } else
        if (ite.m_key == "Image") {
            m_object.image(ite.m_variable);
        } else 
        if (ite.m_key == "Text") {
            m_object.text(ite.m_variable);
        }
    }
}

Object *Widget::object() { return &m_object; }