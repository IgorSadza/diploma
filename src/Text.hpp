#ifndef TEXT_HPP
#define TEXT_HPP

#include "CommonIncludes.hpp"
#include "Font.hpp"
#include "Rectangle.hpp"

class Text {
private:
  Font          m_font; 
  std::string   m_textToRender;
  glm::vec2     m_position;
  glm::vec4     m_color;  

public:
  Text(const std::string &t_renderText);
  Text();

  void font(const std::string &t_fontPath);
  Font font();

  void text(const std::string &t_renderText);
  std::string text();

  void textShif(Rectangle *t_objectRectangle);
  glm::vec2 position();
};
#endif // TEXT_HPP