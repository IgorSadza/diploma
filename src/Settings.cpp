#include "Settings.hpp"

namespace settings {
// window settings
const char *name            = "Diploma Thesis";
const float width           = 800.0f;
const float height          = 600.0f;
const unsigned mouseKeys    = 7;
}; // namespace settings