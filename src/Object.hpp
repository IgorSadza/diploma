/**
    File: Object.hpp
    Purpose: Base to render object.

    @author Igor Sadza
    @version 1.0 - 05/01/19
    @version 2.0 - 12/01/19
*/

#ifndef OBJECT_HPP
#define OBJECT_HPP

#include "CommonIncludes.hpp"

#include "Rectangle.hpp"
#include "Font.hpp"
#include "Image.hpp"
#include "ResourceManager.hpp"
#include "Shader.hpp"
#include "Text.hpp"

class Object {
public:
  // Defines shape of figure.
  enum class Type { RECTANGLE, CIRCLE };

private:
  // Values.
  Type      m_type;
  Rectangle m_rectangle;
  Text      m_text;
  Image     m_image;
  Shader    m_shader;

  std::string m_name;

public:
  Object();
  Object(Type t_type);

  void text(const std::string &t_textToRender) {
    m_text = Text(t_textToRender);
    m_text.textShif(&m_rectangle);
  }

  // void shader();  

  void image(const std::string &t_imagePath) {
    m_image = Image(t_imagePath);
    m_rectangle.size(m_image.size());
  }

  static bool CheckCollision(Rectangle *one, Rectangle *two) // AABB - AABB collision
  {
    // Collision x-axis?
    bool collisionX = one->position()->x + one->size()->x >= two->position()->x &&
                      two->position()->x + two->size()->x >= one->position()->x;
    // Collision y-axis?
    bool collisionY = one->position()->y + one->size()->y >= two->position()->y &&
                      two->position()->y + two->size()->y >= one->position()->y;
    // Collision only if on both axes
    return collisionX && collisionY;
  }

  // Geters.
  Type    type();
  Text    *text();
  Image   *image();
  Shader  *shader();
  Rectangle  *rectangle();
};

#endif // OBJECT_HPP