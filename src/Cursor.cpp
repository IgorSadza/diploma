#include "Cursor.hpp"
#include "SharedResources.hpp"

Object      Cursor::m_object;
bool        Cursor::m_mouseKeys[7];

Cursor::Cursor() {
  for (auto &key : m_mouseKeys) {
    key = GLFW_FALSE;
  }

  GLFWcursor *cursor;
  GLFWimage *image = new GLFWimage();

  image->pixels = stbi_load("res/sprites/cursors/cursor_normal.png", &image->height, &image->width, nullptr, 0);

  cursor = glfwCreateCursor(image, 0, 0);
  glfwSetCursor(shared::window, cursor);

  m_object.rectangle()->size(0.0f);
  m_object.rectangle()->color(1.0f);
}

Cursor &Cursor::init() {
    static Cursor cursor;
    return cursor;
}

Object *Cursor::object() {
    return &m_object;
}

void Cursor::key(int t_key, int t_action) {
    m_mouseKeys[t_key] = t_action;
}

bool Cursor::key(int t_key) {
  // if (m_mouseKeys[t_key]) {
  //   for (auto& ite: m_mouseKeys) {
  //     ite = false;
  //   }
  //   return true;
  // }
  return m_mouseKeys[t_key];
}