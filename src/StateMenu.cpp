#include "StateMenu.hpp"

static StateMenu *stateMenu;

StateMenu::StateMenu() {
  stateMenu = this;
  State::Register("stateMenu", stateMenu);
  State::setInstance("stateMenu");
}

GLvoid StateMenu::logic() {}

GLvoid StateMenu::render() {}