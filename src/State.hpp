#ifndef STATE_HPP_
#define STATE_HPP_

#include "CommonIncludes.hpp"

class State {
private:
  typedef std::map<std::string, State *> StateRegister;
  static StateRegister m_registry;
  static State *m_instance;

public:
  static State *GetInstance();
  static void Register(const std::string &_name, State *_state);
  static void setInstance(const std::string &_name);

  virtual void render() = 0;
  virtual void logic() = 0;

protected:
  static State *LookUp(const std::string &_name);
};
#endif // STATE_HPP_