#ifndef PARTICLES_GENERATOR_HPP
#define PARTICLES_GENERATOR_HPP

#include "CommonIncludes.hpp"
#include "Rectangle.hpp"
#include "Object.hpp"
#include "Shader.hpp"

struct Particle : public Rectangle{
    private:
    public:
        Particle();
        float m_life;
        float m_velocity;
};

class ParticleGenerator {
    private:
        unsigned m_amount;
        unsigned m_lastUsedParticle;
        
        glm::vec2 *m_positionFrom;
        glm::vec2 m_range;

        std::vector<Particle> m_particles;
        
        void updateParticle();
        void respawnParticle();
        Particle *findFirstUnusedParticle();

    public:
        ParticleGenerator(glm::vec2 t_position, glm::vec2 t_range, unsigned t_amount);
        ParticleGenerator(Object &t_object, glm::vec2 t_range,  unsigned t_amount);
        void renderParticles();
};

class ParticleRenderer {
    private:
        ParticleRenderer();
        static unsigned     m_vao;
        static unsigned     m_vbo;
        static Shader       m_shader;
    public:
        static ParticleRenderer &init();
        static void render(Particle *t_particle);
};

#endif //PARTICLES_GENERATOR_HPP