/**
 *  File: ResourceManager.hpp
 *  Purpose:    
 * 
 *  @author: Igor Sadza
 *  @version 1.0 - 31/01/19 
*/

#ifndef RESOURCE_MANAGER_HPP
#define RESOURCE_MANAGER_HPP

#include "CommonIncludes.hpp"

template <class Type>
class ResourceManager {
private:
    static std::vector<Type> m_registry;
    static bool search(Type t_object);
public:
    static bool searchObject(const std::string &t_objectName);
    static void registerObject(const Type &t_object);
    static std::vector<Type> getObjects();
    static Type getObject(const std::string& t_objectName);
};

#include "ResourceManager.hxx"

#endif // RESOURCE_MANAGER_HPP