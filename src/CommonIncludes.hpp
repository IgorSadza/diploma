#pragma ONCE
#ifndef COMMON_INCLUDES_HPP
#define COMMON_INCLUDES_HPP

// Legacy headers
#include <array>
#include <fstream>
#include <iostream>
#include <list>
#include <map>
#include <sstream>
#include <stdlib.h>
#include <time.h>
#include <typeindex>
#include <typeinfo>
#include <utility>
#include <vector>

// Font loader
#include <ft2build.h>
#include FT_FREETYPE_H

// OpenGL loader
#include "dep/glad.h"
// Image loader
#include "dep/stb_image.h"

// OpenGL surface
#include <GLFW/glfw3.h>

// OpenGL mathematics
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#endif // COMMON_INCLUDES_HPP
