#ifndef WIDGET_HPP
#define WIDGET_HPP

#include "CommonIncludes.hpp"
#include "Object.hpp"
#include "TextFileParser.hpp"

class Widget {
    private:
        Object m_object;
    public:
        Widget(TextFileParser::Variables t_variables);
        virtual Object *object();   
        virtual void render() = 0;
        virtual void logic() = 0;
};

#endif //WIDGHET_HPP
