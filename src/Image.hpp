/**
    File: Image.cpp
    Purpose: Base to render object.

    @author Igor Sadza
    @version 1.0 - 19/01/19
*/

#ifndef IMAGE_HPP
#define IMAGE_HPP

#include "CommonIncludes.hpp"

struct Image {
private:
  unsigned     m_id;
  glm::ivec2   m_size;
  std::string  m_name;

public:
  Image(const std::string &t_imagePath);
  Image();
  void bindImage();

  glm::vec2    size();
  std::string  name();
};

#endif // IMAGE_HPP