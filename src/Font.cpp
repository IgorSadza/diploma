#include "Font.hpp"
#include "TextFileParser.hpp"
#include "ResourceManager.hpp"

Font::Font() {}

Font::Font(const std::string& t_pathToFont) { 

  FT_Face face;
  FT_Library library;

  if (FT_Init_FreeType(&library)) {
      std::cout << "Failed to init Library" << std::endl;
      exit (EXIT_FAILURE);
  }

  if (FT_New_Face(library, t_pathToFont.c_str(), 0, &face)) {
      std::cout << "Failed to load font: " + t_pathToFont << std::endl;
      exit (EXIT_FAILURE);
  }

  m_name = TextFileParser::parseName(t_pathToFont);

  FT_Set_Pixel_Sizes(face, 0, 15);
  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

  for (GLubyte ch = 0; ch < 128; ch++) {

    if (FT_Load_Char(face, ch, FT_LOAD_RENDER)) {
      std::cout << "ERROR::FREETYTPE: Failed to load Glyph" << std::endl;
      continue;
    }

    GLuint texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, face->glyph->bitmap.width,
                 face->glyph->bitmap.rows, 0, GL_RED, GL_UNSIGNED_BYTE,
                 face->glyph->bitmap.buffer);

    glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
    glPixelStorei(GL_UNPACK_IMAGE_HEIGHT, 0);
    glPixelStorei(GL_UNPACK_SKIP_PIXELS, 0);
    glPixelStorei(GL_UNPACK_SKIP_ROWS, 0);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    glm::ivec2 size(face->glyph->bitmap.width, face->glyph->bitmap.rows);
    glm::ivec2 bearing(face->glyph->bitmap_left, face->glyph->bitmap_top);

    Glyph character{texture, size, bearing, face->glyph->advance.x};
    m_alphabet[ch] = character;
  }

  glBindTexture(GL_TEXTURE_2D, 0);
  FT_Done_Face(face);
  FT_Done_FreeType(library);

  ResourceManager<Font>::registerObject(*this);
}

std::string Font::name() { return m_name; }

Font::Alphabet Font::alphabet() { return m_alphabet; }