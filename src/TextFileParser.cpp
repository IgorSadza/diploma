/**
    @File:      TextParser.cpp
    @Purpose:   Parse file to section based on their header.
                Analyze given section and create key = variable structure.    

    @Author:    Igor Sadza
    @Version:   1.0 - 30/01/19
                2.0 - 13/02/19
                3.0 - 17/02/19
                4.0 - 18/02/19
*/

#include "TextFileParser.hpp"

TextFileParser::TextFileParser(const std::string &t_filePath)
  : m_filePath(t_filePath) {

  m_fileName = parseName(t_filePath);

  parseSources();  
  parseVariables();
  parseGlobals();
};

std::string TextFileParser::getName() { 
  return m_fileName; 
};

TextFileParser::Sections TextFileParser::getSections() { 
  return m_sections; 
};

TextFileParser::Registry TextFileParser::getVariables() {
  return m_variables;
}

void TextFileParser::parseSources() {

  std::ifstream fileStream;
  fileStream.open(m_filePath);

  if (!fileStream) {
    std::cout << "The file cannot be opened: " + m_filePath  << std::endl;
     exit (EXIT_FAILURE);
  }

  HeadSection headSection;

  std::string line;
  std::string section;
  std::string header;

  while (getline(fileStream, line)) {

    if (line.length() == 0 || !line.find("//")) {
      continue;
    }
    
    if (line.at(0) == '[') {

      if(section.size() > 0) {
        headSection.m_header = header;
        headSection.m_section = section;
        m_sections.push_back(headSection);

        section.clear();
        header.clear();
      }

      header = parseName(line);
      continue;
    }

    section += line + '\n';
  }

  // Check last 
  if (header.size() > 0) {
    headSection.m_header = header;
    headSection.m_section = section;
    m_sections.push_back(headSection);
  }

  fileStream.close();
};

void TextFileParser::parseVariables() {

  for (auto& source: m_sections) {
    
    std::istringstream stringStream;
    stringStream.str(source.m_section); 
       
    std::string line;
    Variables keyVarVec;

    while (getline(stringStream, line)) {

      KeyVariable keyVar;
      std::size_t quotSignPos = line.find_first_of('"');

      if (quotSignPos != std::string::npos) {

        for (int i = 0; i < quotSignPos; i++) {
          if (line[i] == ' ') {
            line.erase(line.begin() + i);
          }
        }

        for (int i = 0; i < 2; i++) {
          line.erase(line.begin() + line.find('"'));
        } 

      }
      else {
        for (int i = 0; i < line.size(); i++) {
            if (line[i] == ' ') {
              line.erase(line.begin() + i);
            }
        }        
      }

      size_t equalSignPos = line.find_first_of('=');

      if (equalSignPos != std::string::npos) {

        keyVar.m_key = line.substr(0, equalSignPos);
        keyVar.m_variable = line.substr(equalSignPos + 1, line.size());
      }

      keyVarVec.push_back(keyVar);
    }

    m_variables.insert(std::make_pair(source.m_header, keyVarVec));
  }
};

void TextFileParser::parseGlobals() {

  Variables globalVariables;

  for(auto&ite:m_variables) {
    if(ite.first == "Global") {
      globalVariables = ite.second;
    }
  }

  bool find = true;
  for (auto &registry :m_variables) {
    for (auto &globVar :globalVariables) {
      for (auto &sectVar :registry.second) {

        if(globVar.m_key == sectVar.m_key) {
          sectVar.m_variable = globVar.m_variable;
          find = true;
          break;
        }
        find = false;

      }
      if (!find) {
        registry.second.push_back(globVar);
      }
      
    }
  }
};

std::string TextFileParser::parseName(const std::string &t_textLine) {

  std::string name = t_textLine;
  for (auto &cha : {'[', '/', ']'}) {
    size_t signPos = name.find_last_of(cha);
    if (signPos != std::string::npos) {
      (cha == '/')
      ? name.erase(name.begin(), name.begin() + signPos + 1)
      : name.erase(name.begin() + signPos);
    }
  }
  return name;
};