#ifndef OBJECT_RENDERER_HPP
#define OBJECT_RENDERER_HPP

#include "CommonIncludes.hpp"
#include "Object.hpp"
#include "Shader.hpp"

class ObjectRenderer {
private:
  ObjectRenderer();
  static unsigned m_vao;
  static unsigned m_vbo;

public:
  static ObjectRenderer &init();
  static void render(Object *t_object);
};

#endif // RENDERER_HPP