#ifndef STATE_SWITCHER_HPP
#define STATE_SWITCHER_HPP

#include "CommonIncludes.hpp"

class StateSwitcher {
    private:
    public:
        StateSwitcher();
        void render();
        void logic();
};
#endif //STATE_SWITCHER_HPP