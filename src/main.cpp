#include "CommonIncludes.hpp"
#include "Settings.hpp"
#include "SharedResources.hpp"
#include "StateSwitcher.hpp"
#include "Cursor.hpp"

namespace classPointers {
  StateSwitcher   *stateSwitcher;
}

namespace input {

void keyPress(GLFWwindow *t_window, int t_key, int t_scanCode, int t_action,
              int t_mods);
void mousePress(GLFWwindow *t_window, int t_key, int t_action, int t_mods);
void mousePosition(GLFWwindow *t_window, double t_xPos, double t_yPos);

} // namespace input

static void initClient() {
  glfwInit();
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

  shared::window = glfwCreateWindow(settings::width, settings::height,
                                    settings::name, NULL, NULL);
  glfwMakeContextCurrent(shared::window);
}

static void eventManager() {
  glfwSetKeyCallback(shared::window, input::keyPress);
  glfwSetMouseButtonCallback(shared::window, input::mousePress);
  glfwSetCursorPosCallback(shared::window, input::mousePosition);
}

static void initOpenGL() {
  gladLoadGLLoader(GLADloadproc(glfwGetProcAddress));
  glViewport(0, 0, settings::width, settings::height);

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

static void mainLoop() {
  classPointers::stateSwitcher = new StateSwitcher();
  Cursor::init();

  GLfloat lastFrame = 0.0f;
  while (!glfwWindowShouldClose(shared::window)) {

    GLfloat currentFrame = glfwGetTime();
    shared::delta_time = currentFrame - lastFrame;
    lastFrame = currentFrame;

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    
    classPointers::stateSwitcher->logic();
    classPointers::stateSwitcher->render();

    glfwSwapBuffers(shared::window);
    glfwPollEvents();
  }
}

static void clearOut() { 
  glfwTerminate(); 
  delete classPointers::stateSwitcher;
}

int main(int argc, char const *argv[]) {
  initClient();
  eventManager();
  initOpenGL();
  mainLoop();
  clearOut();
  return 0;
}

namespace input {

void keyPress(GLFWwindow *t_window, int t_key, int t_scanCode, int t_action,
              int t_mods) {

  if (t_key == GLFW_KEY_ESCAPE && t_action == GLFW_PRESS) {
    glfwSetWindowShouldClose(t_window, GLFW_TRUE);
  }
}

void mousePress(GLFWwindow *t_window, int t_key, int t_action, int t_mods) {
  Cursor::key(t_key, t_action);
}

void mousePosition(GLFWwindow *t_window, double t_xPos, double t_yPos) {
  Cursor::object()->rectangle()->position((float)t_xPos, (float)t_yPos);
}

} // namespace input