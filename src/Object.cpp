/**
    File: Object.cpp
    Purpose: Base to render object.

    @author Igor Sadza
    @version 1.0 - 05/01/19
    @version 2.0 - 12/01/19
*/

#include "Object.hpp"
#include "ResourceManager.hpp"

Object::Object() {}

Object::Object(Type t_type) {
  (m_type == Type::CIRCLE)
  ? m_shader = ResourceManager<Shader>::getObject("circle.shader")
  : m_shader = ResourceManager<Shader>::getObject("rectangle.shader");
}

// * Getters

Object::Type Object::type() { return m_type; }

Text *Object::text() { return &m_text; }

Image *Object::image() { return &m_image; }

Rectangle *Object::rectangle() { return &m_rectangle; }

Shader *Object::shader() { return &m_shader; }