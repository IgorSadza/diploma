#include "StateSwitcher.hpp"
#include "ObjectRenderer.hpp"
#include "TextRenderer.hpp"
#include "ResourceManager.hpp"
#include "TextFileParser.hpp"
#include "Shader.hpp"
#include "Image.hpp"
#include "Font.hpp"
#include "Cursor.hpp"
#include "WidgetButton.hpp"

#include "ParticlesGenerator.hpp"

ParticleGenerator *pr;
std::vector<Widget*> m_objects;
StateSwitcher::StateSwitcher() {

  ObjectRenderer::init();
  TextRenderer::init();
  ParticleRenderer::init();

  Shader rectangleShader("res/shaders/rectangle.shader");
  pr = new ParticleGenerator(glm::vec2(100.0f), glm::vec2(300.0f), 600);

  //Font defaultFont("res/fonts/04b_30.ttf");

  //TextFileParser tx("res/states/play.state");
  //Font font("res/fonts/04b_30.ttf");

  // for (auto& ite: tx.getVariables()) {
  //   for (auto& it: ite.second) {
  //     if (it.m_key == "textScale") {
  //       glm::vec4 x = tx.parseVariable<glm::vec4>(it.m_variable);
  //     }
  //   }

  // }

  Widget *widget;
  TextFileParser wB("res/states/play.state");
  for (auto &ite : wB.getVariables()) {
    if (ite.first == "WidgetButton") {
      widget = new WidgetButton(ite.second);
      m_objects.push_back(widget);      
    }
  }

// obj.figure()->size(250.0f);
// obj.figure()->position(0.0f);
// obj.figure()->size(100.0f);
// obj.figure()->color(1.0f);

// std::cout << obj.figure()->size().x << std::endl;
// obj.size(250.0f, 100.0f);
// obj.image("res/sprites/widgets/button_menu.png");
// obj.size(250.0f, 100.0f);
// obj.position(100.0f);
// obj
// obj.text("xx");
// obj.text().textToRender("xxd");
// obj.text().position(glm::vec2(100.0f));
// std::cout << obj.text().position() << std::endl;
// Image img("res/sprites/widgets/button_menu.png", glm::vec2(100.0f));
// img.bindImage();

// std::cout << img.size().x << std::endl;

// Figure fig("name", Figure::Type::RECTANGLE);
// fig.color(1.0f, 1.0f, 1.0f);
// std::cout << fig.color().w << std::endl;

// Shader *circleShader = new Shader("circle.shader",
// "res/res/shaders/circle.shader");

// Shader *rectangleShader = new Shader("rectangle.shader",
// "res/res/shaders/rectangle.shader"); Shader *textShader = new
// Shader("text.shader", "res/res/shaders/text.shader");

// for (int i = 0; i < 1000; i++) {
//   test = Figure("circle_" + std::to_string(i), figureType::CIRCLE);
//   test.position(300, 100);
//   test.size(200);
//   test.color(0.5f, 0.5f, 0.5f);
//   ResourceManager<Figure>::recordObject(test);
// }

// const std::string c = "1.0, 0.2";

// for(auto& ite: parserUtils::section("RectangleSize = 50.0, 50.0")) {
//   std::cout << ite.first << " " << ite.second << std::endl;
// }

// glm::vec4 x = parserUtils::extract<glm::vec4>(c);
// std::cout << x.x << std::endl;
}

void StateSwitcher::logic() {

  // for (auto& ite: m_objects) {
  //   ite->logic();
  // }
}

void StateSwitcher::render() {

  // for (auto& ite: m_objects) {
  //   ite->render();
  // }

  pr->renderParticles();
}