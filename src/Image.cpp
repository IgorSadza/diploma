/**
    File: Image.cpp
    Purpose: Load image from path and use image.

    @author Igor Sadza
    @version 1.0 - 19/01/19
*/

#include "Image.hpp"
#include "TextFileParser.hpp"
#include "ResourceManager.hpp"

Image::Image(const std::string &t_imagePath) {

  GLubyte *pixels = stbi_load(t_imagePath.c_str(), &m_size.x, &m_size.y, nullptr, 0);

  if (!pixels) {
    std::cout << "The sprite cannot be open: " + t_imagePath << std::endl;
  }

  m_name = TextFileParser::parseName(t_imagePath);

  glGenTextures(1, &m_id);
  glBindTexture(GL_TEXTURE_2D, m_id);

  glPixelStorei(GL_UNPACK_ROW_LENGTH, m_size.x);
  glPixelStorei(GL_UNPACK_IMAGE_HEIGHT, m_size.y);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

  glGenerateMipmap(GL_TEXTURE_2D);

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_size.x, m_size.y, 0, GL_RGBA,
               GL_UNSIGNED_BYTE, pixels);

  glBindTexture(GL_TEXTURE_2D, 0);
  
  ResourceManager<Image>::registerObject(*this);
};

Image::Image() { }

void Image::bindImage() { glBindTexture(GL_TEXTURE_2D, m_id); }

glm::vec2 Image::size() { return m_size; }

std::string Image::name() { return m_name; }